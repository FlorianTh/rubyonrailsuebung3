#Florian Thom s0558101
## RubyOnRailsUebung3-Project written in Ruby with Rails-Framework

* Here you can find a new Webapplication which is ably to create user(-s) and post. The post are associated with users.
* The Website got published on Heroku
    * URL: test-site-4-ft.herokuapp.com
* The Website is open-souce 

##Getting Started


###Main-Versions

* Ruby version: 2.4.1

* Rails version: 5.1.6 

* gem version: 2.6.11

### Download

* change in your project-workspace and do
* $git clone git@bitbucket.org:FlorianTh/rubyonrailsuebung3.git
* switch into the directory

###Installing
* Download gems: $gem install rails -v 5.1.4 --no-rdoc --no-ri
  
* Install with Bundle: bundle install
    * $bundle install --without production will generate postgresql-database (e.g. on heroku)
    * $bundle install will generate sqlite-database 
  
* Secondly start the server with: $bin/rails server
    * for now only possible with sqllite-database
  
* You can now access http://localhost:3000


##Whole dependecies (are normally auto-installed via bundle install)
actioncable (5.1.6, 5.1.4) 
actionmailer (5.1.6, 5.1.4) 
actionpack (5.1.6, 5.1.4) 
actionview (5.1.6, 5.1.4) 
activejob (5.1.6, 5.1.4) 
activemodel (5.1.6, 5.1.4) 
activerecord (5.1.6, 5.1.4) 
activesupport (5.1.6, 5.1.4) 
addressable (2.5.2) 
arel (8.0.0) 
bigdecimal (default: 1.3.0) 
bindex (0.5.0) 
builder (3.2.3) 
bundler (1.16.1) 
bundler-unload (1.0.2) 
byebug (10.0.2) 
capybara (2.18.0) 
childprocess (0.9.0) 
coffee-rails (4.2.2) 
coffee-script (2.4.1) 
coffee-script-source (1.12.2) 
concurrent-ruby (1.0.5) 
crass (1.0.4) 
did_you_mean (1.1.0) 
erubi (1.7.1) 
execjs (2.7.0) 
executable-hooks (1.3.2) 
ffi (1.9.23) 
gem-wrappers (1.3.2, 1.2.7) 
globalid (0.4.1) 
i18n (1.0.1, 0.9.5) 
io-console (default: 0.4.6) 
jbuilder (2.7.0) 
json (default: 2.0.2) 
listen (3.1.5) 
loofah (2.2.2) 
mail (2.7.0) 
method_source (0.9.0) 
mini_mime (1.0.0) 
mini_portile2 (2.3.0) 
minitest (5.11.3, 5.10.1) 
multi_json (1.13.1) 
net-telnet (0.1.1) 
nio4r (2.3.0) 
nokogiri (1.8.2) 
openssl (default: 2.0.3) 
power_assert (0.4.1) 
psych (default: 2.2.2) 
public_suffix (3.0.2) 
puma (3.11.4) 
rack (2.0.5) 
rack-test (1.0.0) 
rails (5.1.6, 5.1.4) 
rails-dom-testing (2.0.3) 
rails-html-sanitizer (1.0.4) 
railties (5.1.6, 5.1.4) 
rake (12.3.1, 12.0.0) 
rb-fsevent (0.10.3) 
rb-inotify (0.9.10) 
rdoc (default: 5.0.0) 
ruby_dep (1.5.0) 
rubygems-bundler (1.4.4) 
rubyzip (1.2.1) 
rvm (1.11.3.9) 
sass (3.5.6) 
sass-listen (4.0.0) 
sass-rails (5.0.7) 
selenium-webdriver (3.11.0) 
spring (2.0.2) 
spring-watcher-listen (2.0.1) 
sprockets (3.7.1) 
sprockets-rails (3.2.1) 
sqlite3 (1.3.13) 
test-unit (3.2.3) 
thor (0.20.0) 
thread_safe (0.3.6) 
tilt (2.0.8) 
turbolinks (5.1.1) 
turbolinks-source (5.1.0) 
tzinfo (1.2.5) 
uglifier (4.1.10) 
web-console (3.6.1) 
websocket-driver (0.6.5) 
websocket-extensions (0.1.3) 
xmlrpc (0.2.1) 
xpath (3.0.0)