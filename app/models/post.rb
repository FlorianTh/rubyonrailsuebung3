class Post < ApplicationRecord

  validates :poster, :presence => true
  validates :title, :presence => true
  validates :content, :presence => true

  belongs_to :user

end
