Rails.application.routes.draw do


  resources :users do
    resources :posts #posts as nested ressource within users
  end

  root 'home#index'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
